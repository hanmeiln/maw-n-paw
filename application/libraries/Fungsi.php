<?php

class Fungsi {
    protected $ci;

    function __construct(){
        $this->ci =& get_instance();
    }

    function user_login(){
        $this->ci->load->model('Account_Model');
        $id = $this->ci->session->userdata('id');
        $user_data = $this->ci->Account_Model->get($id)->row();
        return $user_data;
    }
}
?>