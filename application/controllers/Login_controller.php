<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login_controller extends CI_Controller {
    public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->load->model('LoginModel');
	}
	
	function index(){
		$this->load->view('login/v_login');
	}
	
	function validation(){
		$this->form_validation->set_rules('user_email', 'Email Address', 'required|trim|valid_email');
		$this->form_validation->set_rules('user_password', 'Password', 'required');
		
		if($this->form_validation->run())
		{
			$result = $this->LoginModel->can_login($this->input->post('user_email'), $this->input->post('user_password'));
			if($result == '')
			{
				redirect(base_url().'HomeLoggedIn_controller');
			}
			else
			{
				//$this->session->set_flashdata('message',$result);
				redirect(base_url().'Login_controller');
			}
		}
		else
		{
			$this->index();
		}
	}
}
?>
