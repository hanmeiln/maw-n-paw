<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class Notif_controller extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model('Notif_Model');
			$this->load->helper(array('url'));
		}

		public function index(){
			$data['pet_data'] = $this->Notif_Model->tampil_hewan();
			$this->load->view('templates/header');
			$this->load->view('templates/navbar_notif');
			$this->load->view('notification/v_notif',$data);
		}
		public function petid(){
			$id_pet=$this->uri->segment(3);
			$data['pet_data'] = $this->Notif_Model->tampil_hewanid($id_pet);
			$this->load->view('templates/header');
			$this->load->view('petinfo/v_petinfonotif',$data);
			$this->load->view('templates/footer');
		}
	}
?>