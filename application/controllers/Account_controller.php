<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class Account_controller extends CI_Controller{
		public function __construct(){
				parent::__construct();
				if(!$this->session->userdata('id'))
				{
					redirect(base_url().'Login_controller');
				}
		}
		
		public function index(){
			$data['posts'] = $this->db->get_where('tb_user', ['email' => $this->session->userdata('email')])->row_array();
			//$this->data['posts'] = $this->Account_Model->getAccounts();
			$this->load->view('templates/header');
			$this->load->view('account/v_account', $data);
			$this->load->view('templates/footer');
		}
		
		function logout(){
			$data = $this->session->all_userdata();
			foreach($data as $row => $rows_value)
			{
				$this->session->unset_userdata($row);
			}
			redirect(base_url().'Mawnpaw_controller');
			
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Your profile has been updated! </div>');
		}
	}
?>
