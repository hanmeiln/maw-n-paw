<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class Adopt_controller extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model('M_adopt');
			$this->load->helper(array('url'));
		}

		public function index(){
			$data['pet_data'] = $this->M_adopt->tampil_hewan();
			$this->load->view('templates/header');
			$this->load->view('templates/navbar_adopt');
			$this->load->view('adopt/v_adopt',$data);
		}
		public function petid(){
			$id_pet=$this->uri->segment(3);
			$data['pet_data'] = $this->M_adopt->tampil_hewanid($id_pet);
			$this->load->view('templates/header');
			$this->load->view('petinfo/v_petinfo',$data);
			$this->load->view('templates/footer');
		}
	}
?>