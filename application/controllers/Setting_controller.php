<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class Setting_controller extends CI_Controller{

		public function index(){
			$this->load->view('templates/header');
			$this->load->view('setting/v_setting');
			$this->load->view('templates/footer');
		}
	}
?>