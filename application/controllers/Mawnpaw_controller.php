<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class Mawnpaw_controller extends CI_Controller{

		public function index(){
			$this->load->view('templates/header');
			$this->load->view('home/v_mawnpaw');
			$this->load->view('templates/footer');
		}
	}
?>