<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class About_controller extends CI_Controller{
		public function index(){
			$this->load->view('templates/header');
			$this->load->view('about/v_about');
			$this->load->view('templates/footer');
		}
	}
?>