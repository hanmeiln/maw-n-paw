<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class InputPet_controller extends CI_Controller{
		public function index(){
			$this->load->view('templates/header');
			$this->load->view('inputpet/v_inputPet');
			$this->load->view('templates/footer');
		}
		
		public function input(){
			$data = array(
				'owner_id' 		=>	$this->session->userdata('owner_id'),
				'pet_name'		=>	$this->input->post('pet_name'),
				'pet_gender'	=>	$this->input->post('pet_gender'),
				'type'			=>	$this->input->post('type'),
				'breed'			=>	$this->input->post('breed'),
				'birthday'		=>	$this->input->post('birthday'),
				'weight'		=>	$this->input->post('weight'),
				'description'	=>	$this->input->post('description')
			);
			
			$id_pet = $this->db->insert_id();
			$this->db->insert('pet_data', $data);
			//$this->session->set_userdata('id_pet', $row->id_pet);
			
			redirect(base_url().'AdoptLoggedIn_controller');
		}
	}
?>