<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class AboutLoggedIn_controller extends CI_Controller{
		public function index(){
			$this->load->view('templates/header');
			$this->load->view('about/v_aboutloggedin');
			$this->load->view('templates/footer');
		}
	}
?>