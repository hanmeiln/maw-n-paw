<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class Signup_controller extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('encrypt');
			$this->load->model('Signup_model');
		}

		function index(){
			$this->load->view('signup/v_signup');
		}

		function validation(){
			$this->form_validation->set_rules('user_name', 'Name', 'required|trim');
			$this->form_validation->set_rules('user_email', 'Email Address', 'required|trim|valid_email|is_unique[tb_user.email]');
			$this->form_validation->set_rules('user_password', 'Password', 'required');
			if($this->form_validation->run())
			{
				$verification_key = md5(rand());
				$encrypted_password = $this->encrypt->encode($this->input->post('user_password'));
				$data = array(
					'name'  => $this->input->post('user_name'),
					'email'  => $this->input->post('user_email'),
					'password' => $encrypted_password,
					'verification_key' => $verification_key
				);
				
				$id = $this->Signup_model->insert($data);
				redirect(base_url().'Login_controller');
				
			}
			else
			{
				$this->index();
			}
		}
	}
?>