<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class HomeLoggedIn_controller extends CI_Controller{

		public function index(){
			$this->load->view('templates/header');
			$this->load->view('home/v_homeloggedin');
			$this->load->view('templates/footer');
		}
	}
?>