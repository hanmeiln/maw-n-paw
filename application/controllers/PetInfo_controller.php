<?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class PetInfo_controller extends CI_Controller{
		public function index(){
			$this->load->view('templates/header');
			$this->load->view('petinfo/v_petInfo');
			$this->load->view('templates/footer');
		}
	}
?>