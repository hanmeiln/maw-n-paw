<html>
	<head>
		<title>Sign up - Maw n Paw</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		body{
			background-image: url("<?php echo base_url();?>../foto/bg9.jpg");
		}
		.judul{
			margin-top: 60px;
			margin-right: 25px;
		}
		.judul h1{
			font-family: forte;
			color:#996633;
			font-size:100px;
			margin-bottom:0;
		}
		h2{
			font-family: Bahnschrift SemiBold;
			color:#996633;
			font-size:30px;
			text-align: center;
			margin-top:0;
		}
		.judul{
			float: right;
		}
		
		.login{
			position: absolute;
			bottom: 60px;
			left: 10px;
			width: 40%;
			background: white;
			margin: 20px;
			padding: 10px;
		}
		.jumbotron{
			background: rgba(255,255,255,0.6);
			bottom: 40px;
			left: 50px;
			position: absolute;
			padding: 2px;
			width: 39%;
			height: 43%;
			margin-left: 150px;
		}
		.jumbotron h1{
			font-family: forte;
			font-size: 30px;
		}
		.text-danger{
			font-size: 30px;
		}
	</style>
	</head>
	
	<body>
	<?php
		if($this->session->flashdata('message'))
		{
			echo '
			<div class="alert alert-success">
				'.$this->session->flashdata("message").'
			</div>
			';
		}
	?>
	<div class="judul">
		<h1>Maw n Paw</h1>
		<h2>Adopt a pet today!</h2>
	</div>
	
	<div class="container">
	<div class="jumbotron" style="border-radius: 25px;">
		<h1>Sign up</h1>
		<form class="form-horizontal" action="<?php echo base_url();?>Signup_controller/validation" method="post">
			<div class="form-group">
				<label class="control-label col-sm-2">Username:</label>
				<div class="col-sm-10">
				<input type="text" class="form-control" id="user_name" placeholder="Enter username" name="user_name" value="<?php echo set_value('user_name');?>"/>
					<span class="text-danger"><?php echo form_error('user_name'); ?></span>
				</div>
            </div>
            <div class="form-group">
				<label class="control-label col-sm-2">Email:</label>
                <div class="col-sm-10">          
					<input type="text" class="form-control" id="user_email" placeholder="Enter email" name="user_email" value="<?php echo set_value('user_email'); ?>" />
					<span class="text-danger"><?php echo form_error('user_email'); ?></span>
				</div>
            </div>
			<div class="form-group">
			  <label class="control-label col-sm-2">Password:</label>
			  <div class="col-sm-10">          
				<input type="password" class="form-control" id="user_password" placeholder="Enter password" name="user_password" value="<?php echo set_value('user_password');?>"/>
					<span class="text-danger"><?php echo form_error('user_password'); ?></span>
			  </div>
			</div>
			<div class="form-group">        
			  <div class="col-sm-offset-2 col-sm-10">
			  </div>
			</div>
			<div class="form-group">        
			  <div class="col-sm-offset-2 col-sm-10">
				<input type="submit" class="btn btn-default" name="signup" value="Sign up"/>
			  </div>
			</div>
		</form>
	</div>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	</body>
</html>