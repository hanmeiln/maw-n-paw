<!DOCTYPE HTML>
<html>
	<head>
		<title>Account - Maw n Paw</title>
  <link rel="icon" href="logosquare.png">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<style>
			.navbar{
				list-style: none;
				background: #996633;
				text-align: center;
			}
			.navbar li{
				display: inline-block;
				border-right: 1px solid #ffe6ee;
			}
			.navbar li:last-child{
				border-right: none;
			}
			.navbar a{
				text-decoration: none;
				color: #ffe6ee;
				width: 117px;
				display: block;
				padding: 14px 20px;
				font-size:150%;
				font-family: calibri;
			}
			.navbar ul li a:hover{
				background: #ff80aa;
				transition:0.4s;
				display: block;
			}
			.active{
				background-color: #80d4ff;
			}
			.jumbotron{
				background-color: white;
				font-family: calibri;
				font-size:20px;
				width:900px;
			}
            .container{
                padding-left : 200px;
                padding-right : 200px;
            }
		</style>
	</head>
	
	<body>
	<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="glyphicon glyphicon-menu-down"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<?= base_url().'HomeLoggedIn_controller/index/'?>">Home</a></li>
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Pets <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?= base_url().'CatsLoggedIn_controller/index/'?>">Cats</a></li>
							<li><a href="<?= base_url().'DogsLoggedIn_controller/index/'?>">Dogs</a></li>
							<li><a href="<?= base_url().'AdoptLoggedIn_controller/index/'?>">All</a></li>
						</ul>
					</li>
					<li><a href="<?= base_url().'AboutLoggedIn_controller/index/'?>">About</a></li>
					<li><a href="<?= base_url().'Notif_controller/index/'?>">Notification</a></li>
				</ul>
				
				<ul class="nav navbar-nav navbar-right">
				<li><form class="navbar-form navbar-left" action="/action_page.php">
				  <div class="input-group">
					<input type="text" class="form-control" placeholder="Search" name="search">
					<div class="input-group-btn">
					  <button class="btn btn-default" type="submit">
						<li class="glyphicon glyphicon-search"></li>
					  </button>
					</div>
				  </div>
				</form></li>
					<li class="active"><a href="#">Account</a></li>
				</ul>
			</div>
		</nav>
		
		<div class="container">
			<h1 style="text-align:center; font-family:forte; font-size:50px">Account</h1>
			<br>
			<center>
				<div class="jumbotron" style="border-radius: 25px;">
					<div class="container">
						<center><img src="<?php echo base_url();?>../foto/profile.png" alt="Profile" width=200></center>
						<br>
						<center>
							<table>
								<tbody>
									<tr>
										<td><strong>Name :</strong></td>
										<td><?=$this->fungsi->user_login()->name?></td>
									</tr>
									<tr>
										<td><strong>Email :</strong></td>
										<td><?=$this->fungsi->user_login()->email?></td>
									</tr>
								</tbody>
							</table>
							<br>
							<a href="<?= base_url().'Setting_controller/index/'?>" class="btn btn-warning btn-lg" role="button">Settings</a> <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">Logout</button>
						</center>
					</div>
				</div>
			</center>
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Modal Header</h4>
						</div>
						<div class="modal-body">
							<p>Some text in the modal.</p>
						</div>
						<div class="modal-footer">
							<a class="btn btn-default btn-lg" role="button" data-dismiss="modal">Cancel</a>
							<a href="<?= base_url().'Account_controller/logout/'?>" class="btn btn-danger btn-lg" role="button">Logout</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>