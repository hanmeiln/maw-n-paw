<!DOCTYPE HTML>
<html>
	<head>
		<title>Account - Maw n Paw</title>
  <link rel="icon" href="logosquare.png">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<style>
			.navbar{
				list-style: none;
				background: #996633;
				text-align: center;
			}
			.navbar li{
				display: inline-block;
				border-right: 1px solid #ffe6ee;
			}
			.navbar li:last-child{
				border-right: none;
			}
			.navbar a{
				text-decoration: none;
				color: #ffe6ee;
				width: 117px;
				display: block;
				padding: 14px 20px;
				font-size:150%;
				font-family: calibri;
			}
			.navbar ul li a:hover{
				background: #ff80aa;
				transition:0.4s;
				display: block;
			}
			.active{
				background-color: #80d4ff;
			}
			.jumbotron{
				background-color: white;
				font-family: calibri;
				font-size:20px;
			}
            .container{
                padding-left : 200px;
                padding-right : 200px;
            }
			.picture-container{
    			position: relative;
    			cursor: pointer;
    			text-align: center;
			}
			.picture{
    			width: 200px;
    			height: 200px;
    			background-color: #999999;
    			border: 4px solid #CCCCCC;
    			color: #FFFFFF;
    			border-radius: 50%;
    			margin: 0px auto;
    			overflow: hidden;
    			transition: all 0.2s;
    			-webkit-transition: all 0.2s;
			}
			.picture:hover{
    			border-color: #2ca8ff;
			}
			.content.ct-wizard-green .picture:hover{
    			border-color: #05ae0e;
			}
			.content.ct-wizard-blue .picture:hover{
    			border-color: #3472f7;
			}
			.content.ct-wizard-orange .picture:hover{
    			border-color: #ff9500;
			}
			.content.ct-wizard-red .picture:hover{
    			border-color: #ff3b30;
			}
			.picture input[type="file"] {
    			cursor: pointer;
    			display: block;
    			height: 100%;
    			left: 0;
    			opacity: 0 !important;
    			position: absolute;
    			top: 0;
    			width: 100%;	
			}
			.picture-src{
    			width: 100%; 
			}
			.center{
				padding-left: 200px;
			}
		</style>
	</head>
	
	<body>
	<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="glyphicon glyphicon-menu-down"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<?= base_url().'HomeLoggedIn_controller/index/'?>">Home</a></li>
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Pets <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Cats</a></li>
							<li><a href="#">Dogs</a></li>
							<li><a href="<?= base_url().'AdoptLoggedIn_controller/index/'?>">All</a></li>
						</ul>
					</li>
					<li><a href="<?= base_url().'AboutLoggedIn_controller/index/'?>">About</a></li>
					<li><a href="<?= base_url().'Notif_controller/index/'?>">Notification</a></li>
				</ul>
				
				<ul class="nav navbar-nav navbar-right">
				<li><form class="navbar-form navbar-left" action="/action_page.php">
				  <div class="input-group">
					<input type="text" class="form-control" placeholder="Search" name="search">
					<div class="input-group-btn">
					  <button class="btn btn-default" type="submit">
						<li class="glyphicon glyphicon-search"></li>
					  </button>
					</div>
				  </div>
				</form></li>
					<li class="active"><a href="#">Account</a></li>
				</ul>
			</div>
		</nav>
	
	
	<div class="container">
		<h1 style="text-align:center; font-family:forte; font-size:50px">Account</h1>
        <br>
		<div class="jumbotron" style="border-radius: 25px;">
            <div class="container">
				<div class="picture-container">
        			<div class="picture">
            			<img src="https://lh3.googleusercontent.com/LfmMVU71g-HKXTCP_QWlDOemmWg4Dn1rJjxeEsZKMNaQprgunDTtEuzmcwUBgupKQVTuP0vczT9bH32ywaF7h68mF-osUSBAeM6MxyhvJhG6HKZMTYjgEv3WkWCfLB7czfODidNQPdja99HMb4qhCY1uFS8X0OQOVGeuhdHy8ln7eyr-6MnkCcy64wl6S_S6ep9j7aJIIopZ9wxk7Iqm-gFjmBtg6KJVkBD0IA6BnS-XlIVpbqL5LYi62elCrbDgiaD6Oe8uluucbYeL1i9kgr4c1b_NBSNe6zFwj7vrju4Zdbax-GPHmiuirf2h86eKdRl7A5h8PXGrCDNIYMID-J7_KuHKqaM-I7W5yI00QDpG9x5q5xOQMgCy1bbu3St1paqt9KHrvNS_SCx-QJgBTOIWW6T0DHVlvV_9YF5UZpN7aV5a79xvN1Gdrc7spvSs82v6gta8AJHCgzNSWQw5QUR8EN_-cTPF6S-vifLa2KtRdRAV7q-CQvhMrbBCaEYY73bQcPZFd9XE7HIbHXwXYA=s200-no" class="picture-src" id="wizardPicturePreview" title="">
            			<input type="file" id="wizard-picture" class="">
        			</div>
         			<h6 class="">Choose Profile Picture</h6>
    			</div>
				<div class="center">
				<div class="row">
					<div class="col-lg-10">
						<form>
						<div class="form-group row">
							<label for="name" class="">Name :</label>
							<div class="">
								<input type="text" class="form-control" id="name" name="name" value="<?= $this->fungsi->user_login()->name; ?>">
							</div>
						</div>
						<div class="form-group row">
							<label for="email" class="">Email :</label>
							<div class="">
								<input type="text" class="form-control" id="email" name="email" value="<?= $this->fungsi->user_login()->email; ?>">
							</div>
						</div>

						</form>
					</div>
				</div>
    		</div>
            <center><a href="" class="btn btn-success" role="button">Update Profile</a></center>
        </div>
		</di>
	</div>
	</body>

	<script>
		$(document).ready(function(){
		// Prepare the preview for profile picture
    		$("#wizard-picture").change(function(){
        		readURL(this);
    		});
		});
		function readURL(input) {
    	if (input.files && input.files[0]) {
        	var reader = new FileReader();
			reader.onload = function (e) {
            	$('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        	}
        reader.readAsDataURL(input.files[0]);
    	}
		}
	</script>
</html>