<!DOCTYPE HTML>
<html>
	<head>
		<title>Adoption Form - Maw n Paw</title>
		<link rel="icon" href="logosquare.png">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<style>
			.navbar{
				list-style: none;
				background: #996633;
				text-align: center;
			}
			.navbar li{
				display: inline-block;
				border-right: 1px solid #ffe6ee;
			}
			.navbar li:last-child{
				border-right: none;
			}
			.navbar a{
				text-decoration: none;
				color: #ffe6ee;
				width: 117px;
				display: block;
				padding: 14px 20px;
				font-size:150%;
				font-family: calibri;
			}
			.navbar ul li a:hover{
				background: #ff80aa;
				transition:0.4s;
				display: block;
			}
			.active{
				background-color: #80d4ff;
			}
			.jumbotron{
				background-color: white;
				border-radius: 25px;
				font-family: calibri;
				font-size:20px;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="glyphicon glyphicon-menu-down"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="<?= base_url().'HomeLoggedIn_controller/index/'?>">Home</a></li>
						<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Pets <span class="caret"></span></a>
							<ul class="dropdown-menu">
							<li><a href="<?= base_url().'CatsLoggedIn_controller/index/'?>">Cats</a></li>
							<li><a href="<?= base_url().'DogsLoggedIn_controller/index/'?>">Dogs</a></li>
								<li><a href="<?= base_url().'AdoptLoggedIn_controller/index/'?>">All</a></li>
							</ul>
						</li>
						<li><a href="<?= base_url().'AboutLoggedIn_controller/index/'?>">About</a></li>
						<li><a href="<?= base_url().'Notif_controller/index/'?>">Notification</a></li>
					</ul>
				
					<ul class="nav navbar-nav navbar-right">
						<li>
							<form class="navbar-form navbar-left" action="/action_page.php">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search" name="search">
									<div class="input-group-btn">
										<button class="btn btn-default" type="submit">
											<li class="glyphicon glyphicon-search"></li>
										</button>
									</div>
								</div>
							</form>
						</li>
						<li><a href="<?= base_url().'Account_controller/index/'?>">Account</a></li>
					</ul>
				</div>
			</div>
		</nav>
		
		<div class="container">
			<h1 style="text-align:center; font-family:forte; font-size:50px">Adoption Form</h1>
			<br>
			<div class="jumbotron" style="border-radius: 25px;">
				<div class="container">
					<h2>Please tell us about your pet</h2>
					<br>
					<form class="form-horizontal" action="<?php echo base_url();?>InputPet_controller/input" method="post">
						<div class="form-group">
							<div class="form-group">
								<label class="control-label col-sm-2">Name:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="pet_name" name="pet_name" placeholder="Enter pet name"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2">Gender:</label>
								<div class="col-sm-10">
									<div class="radio">
										<label><input type="radio" name="pet_gender" value="Male"/>Male</label>
									</div>
									
									<div class="radio">
										<label><input type="radio" name="pet_gender" value="Female"/>Female</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2">Type:</label>
								<div class="col-sm-10">
									<div class="radio">
										<label><input type="radio" name="type" value="Cat"/>Cat</label>
									</div>
									
									<div class="radio">
										<label><input type="radio" name="type" value="Dog"/>Dog</label>
									</div>
								</div>
							</div>
							<div class="form-group">	
								<label class="control-label col-sm-2">Breed:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="breed" name="breed" placeholder="Enter pet breed"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2">Birthday:</label>
								<div class="col-sm-10">
									<input type="date" class="form-control" id="birthday" name="birthday"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2">Weight:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="weight" name="weight" placeholder="Enter pet weight"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2">Description:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="description" name="description" placeholder="Write description about your pet"/>
								</div>
							</div>
						</div>		
						<center>
							<a href="<?= base_url().'InputPet_controller/indexOwner/'?>" class="btn btn-info btn-lg" role="button">Back</a>
							<input type="submit" class="btn btn-info btn-lg" name="insert" value="Insert"/>
						</center>
					</form>
				</div> 	
			</div>
		</div>
	</body>
</html>