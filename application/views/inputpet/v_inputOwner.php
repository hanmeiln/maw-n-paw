<!DOCTYPE HTML>
<html>
	<head>
		<title>Adoption Form - Maw n Paw</title>
		<link rel="icon" href="logosquare.png">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<style>
			.navbar{
				list-style: none;
				background: #996633;
				text-align: center;
			}
			.navbar li{
				display: inline-block;
				border-right: 1px solid #ffe6ee;
			}
			.navbar li:last-child{
				border-right: none;
			}
			.navbar a{
				text-decoration: none;
				color: #ffe6ee;
				width: 117px;
				display: block;
				padding: 14px 20px;
				font-size:150%;
				font-family: calibri;
			}
			.navbar ul li a:hover{
				background: #ff80aa;
				transition:0.4s;
				display: block;
			}
			.active{
				background-color: #80d4ff;
			}
			.jumbotron{
				background-color: white;
				border-radius: 25px;
				font-family: calibri;
				font-size:20px;
			}
		</style>
	</head>
	
	<body>
		<nav class="navbar">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="glyphicon glyphicon-menu-down"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="<?= base_url().'HomeLoggedIn_controller/index/'?>">Home</a></li>
						<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Pets <span class="caret"></span></a>
							<ul class="dropdown-menu">
							<li><a href="<?= base_url().'CatsLoggedIn_controller/index/'?>">Cats</a></li>
							<li><a href="<?= base_url().'DogsLoggedIn_controller/index/'?>">Dogs</a></li>
								<li><a href="<?= base_url().'AdoptLoggedIn_controller/index/'?>">All</a></li>
							</ul>
						</li>
						<li><a href="<?= base_url().'AboutLoggedIn_controller/index/'?>">About</a></li>
						<li><a href="<?= base_url().'Notif_controller/index/'?>">Notification</a></li>
					</ul>
				
					<ul class="nav navbar-nav navbar-right">
						<li>
							<form class="navbar-form navbar-left" action="/action_page.php">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search" name="search">
									<div class="input-group-btn">
										<button class="btn btn-default" type="submit">
											<li class="glyphicon glyphicon-search"></li>
										</button>
									</div>
								</div>
							</form>
						</li>
						<li><a href="<?= base_url().'Account_controller/index/'?>">Account</a></li>
					</ul>
				</div>
			</div>
		</nav>
		
		<div class="container">
			<h1 style="text-align:center; font-family:forte; font-size:50px">Adoption Form</h1>
			<br>
			<div class="jumbotron" style="border-radius: 25px;">
				<div class="container">
					<h2>Please tell us about your info</h2>
					<br>
					<form class="form-horizontal" action="<?php echo base_url();?>InputOwner_controller/input" method="post">
						<div class="form-group">
							<label class="control-label col-sm-3">Full Name:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="full_name" name="full_name" placeholder="Enter full name" value="<?php echo set_value('full_name');?>"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">Address:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="address" name="address" placeholder="Enter address" value="<?php echo set_value('address');?>"/>
							</div>
						</div>	
						<div class="form-group">
							<label class="control-label col-sm-3">Phone:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone" value="<?php echo set_value('phone');?>"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">Picture of your id:</label>
							<div class="col-sm-9">
								<input type="file" class="form-control" id="ktp" name="ktp" size="33" value="<?php echo set_value('phone');?>"/>
							</div>
						</div>	
						<center><input type="submit" class="btn btn-info btn-lg" name="next" value="Next"/></center>	
					</form>
				</div> 
				<center>
			</div>
		</div>
	</body>
</html>
