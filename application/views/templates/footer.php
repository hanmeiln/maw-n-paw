<!DOCTYPE HTML>
<html>
	<head>
		<title>Pets - Maw n Paw</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<style>
			.panel-footer{
				left: 0;
				bottom: 0;
				width: 100%;
				color: #0059b3;
				border-top: 1px solid white;
				text-align:center;
				background: none;
			}
			</style>
	</head>
	<body>
		<div class="container">
			<div class="panel-footer">
				<p><span class="glyphicon glyphicon-home"></span> Jl. Sayang No. 339, kel. Sayang, kec. Sayang, Jatinanangor, Sumedang, Jawa Barat, Indonesia 149990</p>
				<p><span class="glyphicon glyphicon-envelope"></span> mawnpaw@gmail.com</p>
				<p><span class="glyphicon glyphicon-phone-alt"></span> (0435)-xxx-xxx</p>
			</div>
		</div>
	</body>
</html>