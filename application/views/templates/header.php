<!DOCTYPE HTML>
<html>
	<head>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>../foto/logosquare.ico">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<style>
			body{
				background: linear-gradient(to bottom, #f9c4d4 51%, #66ccff 109%);
			}
			.header{
				background-image: url("<?php echo base_url();?>../foto/1.jpg");
				text-align: center;
				padding:20px;
			}
			.judul{
				color : white;
				font-size:500%;
				text-shadow: -3px 0 #ff80aa, 0 3px #ff80aa, 3px 0 #ff80aa, 0 -3px #ff80aa;
				font-family:forte;
				margin-bottom:0;
			}
			.subJudul{
				font-family:Bahnschrift SemiBold;
				color:#996633;
				margin-top:0;
			}
			</style>
	</head>
	<body>
		<div class="header">
			<h1 class="judul">Maw n Paw</h1>
			<h1 class="subJudul">Adopt a pet today!</h1>
		</div>
	</body>
</html>