<!DOCTYPE HTML>
<html>
	<head>
		<title>Pets - Maw n Paw</title>
		<link rel="icon" href="logosquare.png">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<style>
		body{
			background-image: url("<?php echo base_url();?>../foto/bg9.jpg");
		}
		.judul{
			margin-top: 60px;
			margin-right: 25px;
		}
		.judul h1{
			font-family: forte;
			color:#996633;
			font-size:100px;
			margin-bottom:0;
		}
		h2{
			font-family: Bahnschrift SemiBold;
			color:#996633;
			font-size:30px;
			text-align: center;
			margin-top:0;
		}
		.judul{
			float: right;
		}
		
		.login{
			position: absolute;
			bottom: 60px;
			left: 10px;
			width: 40%;
			background: white;
			margin: 20px;
			padding: 10px;
		}
		.jumbotron{
			background: rgba(255,255,255,0.6);
			bottom: 40px;
			left: 50px;
			position: absolute;
			padding: 2px;
			width: 39%;
			height: 43%;
			margin-left: 150px;
		}
		.jumbotron h1{
			font-family: forte;
			font-size: 30px;
		}
		</style>
	</head>
	
	<body>
		<div class="judul">
			<h1>Maw n Paw</h1>
			<h2>Adopt a pet today!</h2>
		</div>
	</body>
</html>