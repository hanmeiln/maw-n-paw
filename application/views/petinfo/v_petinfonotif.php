<!DOCTYPE HTML>
<html>
	<head>
		<title>Pet Info - Maw n Paw</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<style>
			.navbar{
				list-style: none;
				background: #996633;
				text-align: center;
			}
			.navbar li{
				display: inline-block;
				border-right: 1px solid #ffe6ee;
			}
			.navbar li:last-child{
				border-right: none;
			}
			.navbar a{
				text-decoration: none;
				color: #ffe6ee;
				width: 117px;
				display: block;
				padding: 14px 20px;
				font-size:150%;
				font-family: calibri;
			}
			.navbar ul li a:hover{
				background: #ff80aa;
				transition:0.4s;
				display: block;
			}
			.active{
				background-color: #80d4ff;
			}
			.jumbotron{
				background-color: white;
				font-family: Bahnschrift SemiBold;
				font-size:24px;
				color: #c579ad; 
			}
			.panel-footer{
				left: 0;
				bottom: 0;
				width: 100%;
				color: #0059b3;
				border-top: 1px solid white;
				text-align:center;
				background: none;
			}
			<!--.table>tbody>tr>td, 
			.table>tbody>tr>th, 
			.table>tfoot>tr>td, 
			.table>tfoot>tr>th, 
			.table>thead>tr>td, 
			.table>thead>tr>th {
			  border:0;
			}-->
			.center {
				display: block;
				margin-left: auto;
				margin-right: auto;
				width: 50%;
			}
			.table>tbody>tr>td{
				color: #c270a8;
			}
		</style>
	</head>
	
	<body>		
	
		<nav class="navbar">
			<div class="container">
			<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="glyphicon glyphicon-menu-down"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<?= base_url().'HomeLoggedIn_controller/index/'?>">Home</a></li>
					<li class="dropdown active"><a class="dropdown-toggle" data-toggle="dropdown">Pets <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?= base_url().'CatsLoggedIn_controller/index/'?>">Cats</a></li>
							<li><a href="<?= base_url().'DogsLoggedIn_controller/index/'?>">Dogs</a></li>
							<li><a href="<?= base_url().'AdoptLoggedIn_controller/index/'?>">All</a></li>
						</ul>
					</li>
					<li><a href="<?= base_url().'AboutLoggedIn_controller/index/'?>">About</a></li>
					<li><a href="<?= base_url().'Notif_controller/index/'?>">Notification</a></li>
				</ul>
				
				<ul class="nav navbar-nav navbar-right">
				<li><form class="navbar-form navbar-left" action="/action_page.php">
				  <div class="input-group">
					<input type="text" class="form-control" placeholder="Search" name="search">
					<div class="input-group-btn">
					  <button class="btn btn-default" type="submit">
						<li class="glyphicon glyphicon-search"></li>
					  </button>
					</div>
				  </div>
				</form></li>
					<li><a href="<?= base_url().'Account_controller/index/'?>">Account</a></li>
				</ul>
			</div>
		</nav>
		
	<?php foreach($pet_data as $pet):?>	
	<div class="container">
		<div class="jumbotron" style="border-radius: 50px;">
		<div class="row">
			<div class="col-sm-5">
				<img src="<?php echo base_url('../foto/'.$pet->petPic)?>" alt="PetPicture" style="width:220px;">
			</div>
			<div class="col-sm-7">
				<table class="table">
					<tbody>
						<tr>
							<td>Name		:</td>
							<td><?php echo $pet->pet_name;?></td>
						</tr>
						<tr>
							<td>Type		:</td>
							<td><?php echo $pet->type;?></td>
						</tr>
						<tr>
							<td>Gender		:</td>
							<td><?php echo $pet->pet_gender;?></td>
						</tr>
						<tr>
							<td>Birthday	:</td>
							<td><?php echo $pet->birthday;?></td>
						</tr>
						<tr>
							<td>Breed		:</td>
							<td><?php echo $pet->breed;?></td>
						</tr>
						<tr>
							<td>Weight		:</td>
							<td><?php echo $pet->weight;?></td>
						</tr>
						<tr>
							<td>Description	:</td>
							<td><?php echo $pet->description;?></td>
						</tr>
						<tr style="text-align: center">
							<td colspan="2"><a href="#" class="btn btn-danger btn-lg" role="button" style="font-size:24px">Delete pet</a></td>
						</tr>
					</tbody>
				</table>
				
			</div>
		</div>
		</div>
	</div>
	<?php endforeach;?>
	</body>
</html>