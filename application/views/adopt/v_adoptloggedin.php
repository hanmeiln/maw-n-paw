<!DOCTYPE HTML>
<html>
	<head>
		<title>Pets - Maw n Paw</title>
		<link rel="icon" href="logosquare.png">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<style>
			.jumbotron{
				background-color: white;
				border-radius: 25px;
				font-family: calibri;
				font-size:20px;
			}

			.flip-card {
  				background-color: transparent;
  				width: 220px;
  				height: 220px;
  				perspective: 1000px;
			}

			.flip-card-inner {
  				position: relative;
  				width: 220px;
  				height: 220px;
  				text-align: center;
  				transition: transform 0.6s;
  				transform-style: preserve-3d;
  				box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
			}

			.flip-card:hover .flip-card-inner {
  				transform: rotateY(180deg);
			}

			.flip-card-front, .flip-card-back {
  				position: absolute;
  				width: 220px;
  				height: 220px;
  				backface-visibility: hidden;
			}

			.flip-card-front {
  				background-color: #bbb;
  				color: black;
			}

			.flip-card-back {
  				background-color: #fde8ee;
  				color: black;
  				transform: rotateY(180deg);
				font-family: Bahnschrift SemiBold;
			}
			.text {
				color: black;
				font-size: 20px;
				position: absolute;
				top: 50%;
				left: 50%;
				-webkit-transform: translate(-50%, -50%);
				-ms-transform: translate(-50%, -50%);
				transform: translate(-50%, -50%);
				text-align: center;
			}
			.panel-footer{
				left: 0;
				bottom: 0;
				width: 100%;
				color: #0059b3;
				border-top: 1px solid white;
				text-align:center;
				background: none;
			}
		</style>
	</head>
	
	<body>		
		<div class="container">
		<h2 style="text-align:center; font-family:Century751 BT">Choose a pet below to find the information about the pet</h2>
		<hr>
		<?php foreach($pet_data as $pet):?>
		<div class ="col-sm-3">
			<div class="flip-card" style="border-radius : 50%;">
			<a href="<?php echo base_url('AdoptLoggedIn_controller/petid/'.$pet->id_pet); ?>">
				  <div class="flip-card-inner">
						<div class="flip-card-front">
							<img src="<?= base_url('../foto/'.$pet->petPic)?>" alt="PetPicture" style="width:220px;">
						</div>
						<div class="flip-card-back">
							<div class="text">
							<?php echo $pet->pet_name;?>
							</div>
  						</div>
					
				</div>
			</div>
			<br>
		</div>
		<?php endforeach;?>
	</div>

	<br>
	
	<div class="container">	
	<div class="panel-footer">
		<p><span class="glyphicon glyphicon-home"></span> Jl. Sayang No. 339, kel. Sayang, kec. Sayang, Jatinanangor, Sumedang, Jawa Barat, Indonesia 149990</p>
		<p><span class="glyphicon glyphicon-envelope"></span> mawnpaw@gmail.com</p>
		<p><span class="glyphicon glyphicon-phone-alt"></span> (0435)-xxx-xxx</p>
	</div>
	</div>
	
	</body>
</html>